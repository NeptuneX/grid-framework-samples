﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading;
using Grid.Common.Process;

namespace Samples
{
    public class Hello : NodeTask
    {
        protected override void Run()
        {
            Context.Journal.Info("Hi there,  I'm the Hello part");

            var startTime = DateTime.Now;
            while(DateTime.Now.Subtract(startTime).TotalMinutes < 5)
            {
                for(var i=0;i<500;i++)
                    Context.Write<string>(" .. Hello ");
                Thread.Sleep(500);
            }
            Context.Write<string>("bye");
            Context.Journal.Info("Goodbye.");
        }
    }

    public class World : NodeTask
    {
        int i = 0;
        protected override void Run()
        {
            Context.Journal.Info("Hi there!");
            while (true)
            {
                var tmp= Context.Read<string>();
                if (tmp.ToLower() != "bye")
                {
                    if (++i % 5 == 0)
                        Context.Journal.Info($"Got {i} hello messages");
                }
                else
                    break;
            }
            Context.Journal.Info("Goodbye.");
        }
    }
}
