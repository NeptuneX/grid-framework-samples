﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Grid.Common.Process;

namespace Samples.Database
{
    class DataWriter : NodeTask
    {
        string _name;

        protected override void Init()
        {
            base.Init();

            var key = Guid.NewGuid();

            foreach(var ch in key.ToString().ToCharArray())
            {
                if (Char.IsLetterOrDigit(ch) && !_name.ToCharArray().Contains(ch))
                    _name += ch;
            }

            Context.Journal.Info($"File Writer Key: {_name}");

        }
        protected override void Run()
        {
            var obj = Context.Read<PersonEntity>();

            if (obj.Id < 0)
            {
                Context.Journal.Info($"Finished processing!");
                Context.Exit();
            }
            else
            {
                obj.Address = $"Processed by {_name}";
                Context.Repeat();
            }
        }
    }
}
