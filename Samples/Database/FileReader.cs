﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using Grid.Common.Process;

namespace Samples.Database
{
    class FileReader : NodeTask
    {
        readonly List<PersonEntity> persons = new List<PersonEntity>(100);
        const string FILE = "data.csv";

        protected override void Init()
        {
            base.Init();

            if (!File.Exists(FILE))
                throw new Exception($"Cannot find the file {FILE}");

            using (var reader = new StreamReader(FILE))
            {
                while (!reader.EndOfStream)
                {
                    try
                    {
                        var text = reader.ReadToEnd();
                        var data = text.Split(new char[] { '|' }, StringSplitOptions.RemoveEmptyEntries);

                        var person = new PersonEntity();
                        person.Id = persons.Count+1;
                        person.FirstName = data[0].Split(' ')[0];
                        person.LastName = data[0].Split(' ')[1];
                        person.Address = data[1];

                        persons.Add(person);
                    }
                    catch (Exception ex)
                    {
                        Context.Journal.Error(ex, "Problem in reading file");
                    }
                }
            }
        }

        protected override void Run()
        {
            foreach(var p in persons)
            {
                Context.Write<PersonEntity>(p);
            }
            Context.Write<PersonEntity>(new PersonEntity { Id = -1 });

            Context.Exit();
        }
    }
}
