﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using Grid.Common.Process;

namespace Samples.RecordKeeping
{
    class Consumer : NodeTask
    {
        StreamWriter _stream;
        protected override void Init()
        {
            base.Init();

            var location = Path.Combine(Environment.GetFolderPath(Environment.SpecialFolder.Desktop), "Output.txt");

            _stream = new StreamWriter( File.Create(location, 4096));
            _stream.AutoFlush = true;

        }
        protected override void Run()
        {
            var repeat = true;

            var max = 50;
            var list = new List<Person>(max);
            while (repeat)
            {
                var obj = Context.Read<Person>();

                repeat=obj.Id != -1;

               list.Add(obj);
            }

            foreach(var obj in list)
                _stream.WriteLine($"{obj.Id} | {obj.Name} | {obj.Notes}");
        }

        protected override void Destroy()
        {
            base.Destroy();
            if (_stream != null)
            {
                _stream.Flush();
                _stream.Close();
                _stream.Dispose();
                _stream = null;
            }
        }
    }
}
