﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Grid.Common.Process;

namespace Samples.RecordKeeping
{
    class Producer : NodeTask
    {
        List<string> _names = new List<string>();

        protected override void Init()
        {
            base.Init();
            var names = new string[]
            {
                "Kevin   Edwards",
                "Arthur  Bell",
                "James   King",
                "Craig   Gonzalez",
                "Rose    Price",
                "Angela  Cox",
                "Wanda   Alexander",
                "Theresa Perry",
                "Eugene  Davis",
                "Lisa    Cook",
                "Judith  Ward",
                "Billy   Collins",
                "Brian   Murphy",
                "Deborah Scott",
                "Carolyn Allen",
                "Annie   Henderson",
                "Fred    Lewis",
                "Jack    Butler",
                "Patricia Bryant",
                "Amanda  Johnson",
                "Samuel  Hughes",
                "Ernest  Torres",
                "Sandra  Lopez",
                "Martin  Patterson",
                "Sharon  Gray"
            };

            var first = names.Select(x => x.Split(' ').First().Trim());
            var last = names.Select(x => x.Split(' ').Last().Trim());

            foreach (var f in first)
                foreach (var s in last)
                    _names.Add($"{f} {s}");

            Context.Journal
                .Info($"Loaded {_names.Count} have been created");
        }
        protected override void Run()
        {
            for(var i= 0; i < _names.Count; i++)
            {
                var obj = new Person { Id = i, Name = _names[i] };
                Context.Write<Person>(obj);

                if (i % 100 == 0)
                    Context.Journal.Info($"sent out {i / 100} bunch of {((i % 100) == 0 ? 100: i%100)}");
            }

            Context.Write<Person>(new Person { Id = -1 });

            Context.Exit();
        }
    }
}
